/*
 * @author Jharyd Marhkain
 * MQ Student ID: 44784694
 * cactus??
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Canvas extends JPanel implements KeyListener {

   protected static Stage stage;
   private Grid grid;

   public Canvas(){
      setPreferredSize(new Dimension(1280, 720));
      stage = Stage.getInstance();
   }

   @Override
   public void paint(Graphics g) {
      stage.update();
      stage.paint(g, getMousePosition());
   }


   /* implemented from java.awt.event.KeyListener; */
   @Override
   public void keyTyped(KeyEvent e){
      stage.notifyAll(e.getKeyChar(), stage.grid);
   }

   @Override
   public void keyPressed(KeyEvent e){

   }

   @Override
   public void keyReleased(KeyEvent e){

   }

}//eof
