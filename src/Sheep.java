/*
 * @author Jharyd Marhkain
 * MQ Student ID: 44784694
 * cactus??
 */


import java.awt.*;
import java.util.Optional;

public class Sheep extends Character {

   public Sheep(Cell location, Behaviour moveBehaviour){
      super(location, moveBehaviour);
      display = Optional.of(Color.WHITE);
   }

}// eof
