/*
 * @author Jharyd Marhkain
 * MQ Student ID: 44784694
 * cactus??
 */

import bos.*;

import java.awt.*;
import java.util.Optional;

public class RabbitAdapter extends Character {

   protected Rabbit rabbit;

   public RabbitAdapter(Cell location, Behaviour behaviour) {
      super(location, behaviour);
      rabbit = new Rabbit();
      display = Optional.of(Color.lightGray);
   }

   @Override
   public RelativeMove aiMove(Stage stage){
      return moveBehaviour.chooseMove(stage, this);
   }

}//eof
