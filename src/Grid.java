/*
 * @author Jharyd Marhkain
 * MQ Student ID: 44784694
 * cactus??
 */

import bos.*;
//import bos.RelativeMove;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class Grid implements GameBoard<Cell> {

   private int xPos, yPos;
   private long stillMouseTime;
   private Point lastSeenMousePos;
   private Cell cells[][] = new Cell[Main.cellCount][Main.cellCount];

   public Grid(int x, int y) {
      this.xPos = x;
      this.yPos = y;

      for (int row = 0; row < Main.cellCount; row++) {
         for (int col = 0; col < Main.cellCount; col++) {
            cells[row][col] = new Cell(xPos + col * Main.cellSize, yPos + row * Main.cellSize);
         }
      }
   }

   public void paint(Graphics g, Point mousePosition) {

      if (lastSeenMousePos != null && lastSeenMousePos.equals(mousePosition)){
         stillMouseTime++;
      } else {
         stillMouseTime = 0;
      }
      /* lambda expression */
      doToEachCell((c) -> {
         c.paint(g, c.contains(mousePosition));
      });
      doToEachCell((c) -> {
         if (c.contains(mousePosition)){
            if (stillMouseTime > Main.cellCount){
               g.setColor(Color.YELLOW);
               g.fillRoundRect(mousePosition.x + Main.cellCount, mousePosition.y + Main.cellCount, 50, 15, 3, 3);
               g.setColor(Color.BLACK);
               g.drawString("grass: " + c.getGrassHeight(), mousePosition.x + Main.cellCount, mousePosition.y + 32);
            }
         }
      });
      lastSeenMousePos = mousePosition;
   }

   /* random Cell selector for 'Characters' */
   public Cell getRandomCell() {
      java.util.Random rand = new java.util.Random();
      return cells[rand.nextInt(Main.cellCount)][rand.nextInt(Main.cellCount)];
   }

   public Cell cellAtRowCol(Integer row, Integer col){
      return cells[row][col];
   }

   /* implemented from bos.jar */
   private Pair<Integer, Integer> indexOfCell(Cell c) {
      for (int y = 0; y < Main.cellCount; y++) {
         for (int x = 0; x < Main.cellCount; x++) {
            if (cells[y][x] == c) {
               return new Pair(y, x);
            }
         }
      }
      return null;
   }

   private void doToEachCell(Consumer<Cell> func){
      for (int y = 0; y < Main.cellCount; y++) {
         for (int x = 0; x < Main.cellCount; x++) {
            func.accept(cells[y][x]);
         }
      }
   }

   /** Takes a cell predicate (i.e. a function that has a single `Cell` argument and returns a `boolean` result)
    * and applies that predicate to each cell, returning the first cell it finds for which the predicate is true.
    * @param predicate The `Cell` to `boolean` function to test with
    * @return The first cell (searching row by row, left to right) that is true for the predicate.  Returns `null`
    * if no such cell found.
    */
   public Pair<Integer, Integer> findAmongstCells(Predicate<Cell> predicate){
      for (int y = 0; y < Main.cellCount; y++) {
         for (int x = 0; x < Main.cellCount; x++) {
            if (predicate.test(cells[y][x])) {
               return new Pair(y, x);
            }
         }
      }
      return null;
   }

   /** Takes a cell predicate (i.e. a function that has a single `Cell` argument and returns a `result` and applies
    * that predicate to each cell, returning the first cell it finds for which the predicate is true.
    * Returns an optional that is full if such a cell is found and an empty optional if there is no such cell.
    * @param predicate The `Cell` to `boolean` function to test with
    * @return The first cell (searching row by row, left to right) that is true for the predicate.  Returns an empty
    * optional if no cell found.
    */
   public Optional<Pair<Integer, Integer>> safeFindAmongstCells(Predicate<Cell> predicate){
      for (int y = 0; y < Main.cellCount; y++){
         for (int x = 0; x < Main.cellCount; x++){
            if (predicate.test(cells[y][x]))
               return Optional.of(new Pair(y, x));
         }
      }
      return Optional.empty();
   }

   /* using lambdas */
   @Override
   public Optional<Cell> below(Cell relativeTo){
      return safeFindAmongstCells((c)-> c == relativeTo)
            .filter((pair)-> pair.first < 19)
            .map((pair)-> cells[pair.first+1][pair.second]);
   }

   @Override
   public Optional<Cell> above(Cell relativeTo){
      return safeFindAmongstCells((c)-> c == relativeTo)
            .filter((pair)-> pair.first > 0)
            .map((pair)-> cells[pair.first-1][pair.second]);
   }

   @Override
   public Optional<Cell> rightOf(Cell relativeTo){
      return safeFindAmongstCells((c)-> c == relativeTo)
            .filter((pair)-> pair.first < 19)
            .map((pair)-> cells[pair.first][pair.second+1]);
   }

   @Override
   public Optional<Cell> leftOf(Cell relativeTo){
      return safeFindAmongstCells((c)-> c == relativeTo)
            .filter((pair)-> pair.first > 0)
            .map((pair)-> cells[pair.first][pair.second-1]);
   }

   @Override
   public List<RelativeMove> movesBetween(Cell from, Cell to, GamePiece<Cell> mover) {
      Pair<Integer, Integer> fromIndex = findAmongstCells((c)-> c == from);
      Pair<Integer, Integer> toIndex = findAmongstCells((c)-> c == to);

      List<RelativeMove> result = new ArrayList<RelativeMove>();

      /* horizontal movement */
      if (fromIndex.second <= toIndex.second){
         // move right
         for (int i = fromIndex.second; i < toIndex.second; i++){
            result.add(new MoveRight(this, mover));
         }
      } else {
         // move left
         for (int i = toIndex.second; i < fromIndex.second; i++){
            result.add(new MoveLeft(this, mover));
         }
      }

      /* vertical movement */
      if (fromIndex.first <= toIndex.first){
         // move down
         for (int i = fromIndex.first; i < toIndex.first; i++){
            result.add(new MoveDown(this, mover));
         }
      } else {
         // move up
         for (int i = toIndex.first; i < fromIndex.first; i++){
            result.add(new MoveUp(this, mover));
         }
      }
      return result;
   }

}// end of Grid