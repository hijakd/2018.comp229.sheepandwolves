/*
 * @author Jharyd Marhkain
 * MQ Student ID: 44784694
 * cactus??
 */

/* construct one "Grid" and multiple "Character"s */
/* seed = (int)(Math.random()*20); */

//import bos.*;

import java.awt.*;
import java.time.*;
import java.util.*;
import java.util.List;

public class Stage extends KeyObservable {

   private static Stage stageInstance;
   protected Grid grid;
   protected Character sheep;
   protected Character shepherd;
   protected Character wolf;
   protected Player player;
   protected RabbitAdapter rabbit;
   protected SAWReader sReader; // declaring a field to implement SAWReader as a Singleton
   private Instant timeOfLastMove = Instant.now();
   private List<Character> allCharacters;

   private Stage() {
      sReader = SAWReader.getInstance("data/stage1.saw"); // implementing SAWReader as a Singleton
      grid = new Grid(Main.pad, Main.pad);
      /* shepherd must be constructed before sheep, else shepherd moveBehaviour is null */
      shepherd = new Shepherd(grid.cellAtRowCol(sReader.getShepherdLoc().first, sReader.getShepherdLoc().second), new StandStill());
      sheep = new Sheep(grid.cellAtRowCol(sReader.getSheepLoc().first, sReader.getSheepLoc().second), new MoveTowards(shepherd));
      wolf = new Wolf(grid.cellAtRowCol(sReader.getWolfLoc().first, sReader.getWolfLoc().second), new MoveTowards(sheep));
      player = Player.getInstance(grid.getRandomCell());
      rabbit = new RabbitAdapter(grid.getRandomCell(), new RabbitMove());
      this.register(player);

      allCharacters = new ArrayList<Character>();
      allCharacters.add(sheep);
      allCharacters.add(shepherd);
      allCharacters.add(wolf);
      allCharacters.add(rabbit);
   }

   public static Stage getInstance(){
      if (stageInstance == null){
         stageInstance = new Stage();
      }
      return stageInstance;
   }

   public void paint(Graphics g, Point mousePosition) {
      grid.paint(g, mousePosition);
      sheep.paint(g);
      shepherd.paint(g);
      wolf.paint(g);
      rabbit.paint(g);
      player.paint(g);
   }

   public void update() {
      if (!player.inMove()){
         if (sheep.location == shepherd.location) {
            System.out.println("Sheep is safe!!");
            System.exit(0);
         } else if (sheep.location == wolf.location){
            System.out.println("Sheep is dead :(");
            System.exit(1);
         } else {
            if (sheep.location.x == sheep.location.y){
                sheep.setBehaviour(new StandStill());
                shepherd.setBehaviour(new MoveTowards(sheep));
            }
            allCharacters.forEach((c)-> c.aiMove(this).perform());
            player.startMove();
            timeOfLastMove = Instant.now();
         }
      }
   }

}// eof