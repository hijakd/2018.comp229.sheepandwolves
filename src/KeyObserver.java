import bos.GameBoard;

public interface KeyObserver {

   void notify(char c, GameBoard<Cell> gb);

}
