/*
 * @author Jharyd Marhkain
 * MQ Student ID: 44784694
 * cactus??
 */

import bos.*;

//import java.util.List;

public class RabbitMove implements Behaviour {

   private Rabbit rabbitMove = new Rabbit();

   @Override
   public RelativeMove chooseMove(Stage stage, Character mover) {
      int move = rabbitMove.nextMove();

      switch (move){
         // move left
         case 0: new MoveLeft(stage.grid, mover);
            break;
         // move right
         case 1: new MoveRight(stage.grid, mover);
            break;
         // move up
         case 2: new MoveUp(stage.grid, mover);
            break;
         // move down
         case 3: new MoveDown(stage.grid, mover);
            break;
      }
//      return movesToTarget;
      return new NoMove(stage.grid, mover);
   }

}//eof
