/*
 * @author Jharyd Marhkain
 * MQ Student ID: 44784694
 * cactus??
 */

import bos.MoveRandomly;
import bos.NoMove;
import bos.RelativeMove;

import java.awt.*;
import java.util.List;
import java.util.Optional;

public class Wolf extends Character {

   public Wolf(Cell location, Behaviour moveBehaviour) {
      super(location, moveBehaviour);
      display = Optional.of(Color.RED);
   }

//   @Override
//   public RelativeMove aiMove(Stage stage) {
//      List<RelativeMove> possibles = stage.grid.movesBetween(this.location, stage.sheep.location, this);
//      if (possibles.size() == 0) {
//         return new NoMove(stage.grid, this);
//      } else {
//         return possibles.get(0);
//      }
//   }

}// eof