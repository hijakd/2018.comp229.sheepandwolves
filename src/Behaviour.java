/*
 * @author Jharyd Marhkain
 * MQ Student ID: 44784694
 * cactus??
 */

import bos.RelativeMove;

public interface Behaviour {

   RelativeMove chooseMove(Stage stage, Character mover);

}//eof
