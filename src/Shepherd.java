/*
 * @author Jharyd Marhkain
 * MQ Student ID: 44784694
 * cactus??
 */


import bos.NoMove;
import bos.RelativeMove;
import java.awt.*;
import java.util.List;
import java.util.Optional;

public class Shepherd extends Character {

   public Shepherd(Cell location, Behaviour moveBehaviour){
      super(location, moveBehaviour);
      display = Optional.of(Color.GREEN);
   }

//   @Override
//   public RelativeMove aiMove(Stage stage){
//      List<RelativeMove> possibles = stage.grid.movesBetween(this.location, stage.sheep.location, this);
//      return new NoMove(stage.grid, this);
//   }

}// eof
