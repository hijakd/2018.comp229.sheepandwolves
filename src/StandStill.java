/*
 * @author Jharyd Marhkain
 * MQ Student ID: 44784694
 * cactus??
 */

import bos.NoMove;
import bos.RelativeMove;

public class StandStill implements Behaviour {

   @Override
   public RelativeMove chooseMove(Stage stage, Character mover) {
      return new NoMove(stage.grid, mover);
   }

}// eof
