/*
 * @author Jharyd Marhkain
 * MQ Student ID: 44784694
 * cactus??
 */


import java.awt.*;
import java.awt.event.*;
import java.time.Duration;
import java.time.Instant;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Main extends JFrame implements Runnable {

   protected static int pad = 10;
   protected static int cellSize = 35;
   protected static int cellCount = 20;


   public static void main(String[] args) {

      System.out.println("~~~~~");
      System.out.println("Sheep and Wolves");
      System.out.println("~~~~~");

      Main window = new Main();
      window.run();
   }

   private Main() {
      this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      Canvas canvas = new Canvas();
      this.setContentPane(canvas);
      this.addKeyListener(canvas);
      this.pack();
      this.setVisible(true);

   }

   @Override
   public void run() {
      while (true) {
         /* set a start time before program logic */
         Instant startTime = Instant.now();
         this.repaint();
         /* set end time of program logic for Thread.sleep to occur */
         Instant endTime = Instant.now();
         try {
            Thread.sleep(20 - Duration.between(startTime, endTime).toMillis());
         } catch (InterruptedException e) {
            System.out.println("Thread was interrupted, but who cares??");
            System.out.println(e.toString());
         }
      }
   }

}// eof