/*
 * @author Jharyd Marhkain
 * MQ Student ID: 44784694
 * cactus??
 */

//import bos.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SAWReader {

   List<String> contents;
   private static SAWReader sawInstance;

   private SAWReader(String filename) {
      try {
         contents = java.nio.file.Files.readAllLines(java.nio.file.Paths.get(filename));
      } catch (IOException e) {
         /* if exception occurs create empty ArrayList */
         contents = new ArrayList();
      }
   }

   public static SAWReader getInstance(String filename){
      if (sawInstance == null){
         sawInstance = new SAWReader(filename);
      }
      return sawInstance;
   }

   private bos.Pair<Integer, Integer> searchForPair(String target){
      for (String s : contents) {
         Pattern p = Pattern.compile(target + ":\\s*\\((\\d*),\\s*(\\d*)\\)");
         Matcher m = p.matcher(s);
         if (m.matches()) {
            return new bos.Pair(Integer.parseInt(m.group(1).trim()),
                            Integer.parseInt(m.group(2).trim()));
         }
      }
      /* if no Pair found set default position */
      return new bos.Pair(0, 0);
   }

   public bos.Pair<Integer, Integer> getSheepLoc(){
      return searchForPair("sheep");
   }

   public bos.Pair<Integer, Integer> getShepherdLoc() {
      return searchForPair("shepherd");
   }

   public bos.Pair<Integer, Integer> getWolfLoc() {
      return searchForPair("wolf");
   }

//   public Pair<Integer, Integer> getBlockLoc() {
//      return searchForPair("block");
//   }

//   public Pair<Integer, Integer> getBeeLoc() {
//      return searchForPair("bee");
//   }

}// eof
