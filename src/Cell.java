/*
 * @author Jharyd Marhkain
 * MQ Student ID: 44784694
 * cactus??
 */

import java.awt.*;
import java.util.Random;

public class Cell extends Rectangle {

   private static Random rand = new Random();
   Color c;

   public Cell(int x, int y) {
      super(x, y, Main.cellSize, Main.cellSize);
      c = new Color(rand.nextInt(30), rand.nextInt(155)+100, rand.nextInt(30));
   }

   public void paint(Graphics g, Boolean highlighted) {
      /* check if Cell is highlighted */
      if (highlighted) {
         /* draw Cell highlighted */
         g.setColor(Color.LIGHT_GRAY);
         g.fillRect(x, y, Main.cellSize, Main.cellSize);
      } else {
         /* draw Cell without highlighting */
         g.setColor(Color.DARK_GRAY);
         g.fillRect(x, y, Main.cellSize, Main.cellSize);
      }
      /* draw Cell outline */
      g.setColor(Color.BLACK);
      g.drawRect(x, y, Main.cellSize, Main.cellSize);
   }

   @Override
   public boolean contains(Point target){
      if (target == null){
         return false;
      }
      return super.contains(target);
   }

   public int getGrassHeight(){
      return c.getGreen()/50;
   }
}// end of Cell