/*
 * @author Jharyd Marhkain
 * MQ Student ID: 44784694
 * cactus??
 */

import bos.*;
import java.awt.*;
//import java.util.Map;
import java.util.Optional;
//import java.util.List;

public abstract class Character implements GamePiece<Cell> {

   Optional<Color> display;
   Cell location;
   Behaviour moveBehaviour;

   public Character(Cell location, Behaviour behaviour){
      this.location = location;
      this.display = Optional.empty();
      this.moveBehaviour = behaviour;
   }

   public void paint(Graphics g){
      if (display.isPresent()){
         g.setColor(display.get());
         g.fillOval(location.x + location.width/4, location.y + location.height/4,
               location.width/2, location.height/2);
      }
   }

   /* implemented for bos.GamePiece */
   public void setLocationOf(Cell pos){
      this.location = pos;
   }

   /* implemented for bos.GamePiece */
   public Cell getLocationOf(){
      return this.location;
   }

   public void setBehaviour(Behaviour behaviour){
      this.moveBehaviour = behaviour;
   }

   /* implemented for bos.RelativeMove */
   public RelativeMove aiMove(Stage stage){
      return moveBehaviour.chooseMove(stage, this);
   }

} // eof